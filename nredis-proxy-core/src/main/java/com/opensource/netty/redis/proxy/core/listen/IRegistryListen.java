/**
 * 
 */
package com.opensource.netty.redis.proxy.core.listen;

import java.util.List;
import java.util.Map;

/**
 * @author liubing
 *
 */
public interface IRegistryListen {
	
	/**
	 * 监听处理数据变化
	 * @param dataPath 路径
	 * @param data 值
	 */
	public void handleDataChange(String dataPath, Object data);
	
	/**
	 * 处理主节点对应从变化
	 * @param parentPath 主节点路径
	 * @param data 主节点值
	 * @param childPath 从节点路径集合
	 * @param ChildDatas 从节点值
	 */
	public void handleChildChange(String parentPath,String data,List<String> childPath, Map<String, String> slaveMap);
	
	
	/**
	 * 监听从节点处理数据变化
	 * @param dataPath 路径
	 * @param data 值
	 */
	public void handleSlaveDataChange(String dataPath, Object data);
	
}
